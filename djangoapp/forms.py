from django import forms
from djangoapp.models import Post


class PostForm(forms.ModelForm):
    class Meta:
        # model = Post
        fields = '__all__'
