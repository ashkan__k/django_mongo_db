from django.urls import path, include
from . import views

urlpatterns = [
    # path('',views.index,name='index'),
    path('post/create', views.create_new_post),
    path('post/create/with-tags', views.create_tags_and_sync_with_posts),
]
