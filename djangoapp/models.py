from django.db import models
from mongoengine import Document, fields
import mongoengine


class Category(Document):
    title = fields.StringField(max_length=70, null=True, default='')


class Tag(Document):
    name = fields.StringField(max_length=70, null=True, default='')


class Post(Document):
    title = fields.StringField(max_length=70, null=True, default='')
    description = fields.StringField(max_length=200, null=True, default='')
    published = fields.BooleanField(default=False)
    category = fields.ReferenceField(Category, null=True)
    tags = fields.ListField(fields.ReferenceField(Tag, reverse_delete_rule=mongoengine.CASCADE))
