from django.http import HttpResponse
from djangoapp.models import Post, Category, Tag


def create_new_post(request):
    # new_post = Post(
    #     title='New Post',
    #     description='this is mac description',
    #     published=False,
    #     # short_text="this is short text"
    # )

    # Category
    category = Category(title='this is test category')
    category.save()

    data = {
        'title': 'New Post',
        'description': 'this is mac description',
        'published': False,
    }
    new_post = Post(**data)
    new_post.category = category
    new_post.save()

    # Delete
    Post.delete(new_post)

    # work with collections
    post_collection = Post._get_collection()
    post_collection.insert({
        'title': 'tesssst',
        'description': 'tesssst',
        'published': False,
    })

    return HttpResponse(f"<h1>'{new_post.title}' created successfully</h1>")


def create_tags_and_sync_with_posts(request):
    data = {
        'title': 'New Post with tags',
        'description': 'this is mac description',
        'published': True,
    }
    new_post = Post(**data)
    new_post.save()

    # Tags
    tag_1 = Tag(name='this is tag one')
    tag_2 = Tag(name='this is tag two')
    tag_1.save()
    tag_2.save()

    # add new tags to the new_post
    new_post.tags = [tag_1, tag_2]
    new_post.save()

    print('ccccccccccccccccccccccccccccccccc')
    print(new_post.tags)

    return HttpResponse(f"<h1>'{new_post.title}' created successfully</h1>")
