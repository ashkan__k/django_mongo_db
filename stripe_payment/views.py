from django.shortcuts import render, redirect
import stripe
from django.conf import settings
from django.http import JsonResponse
from django.views import View

stripe.api_key = settings.STRIPE_SECRET_KEY


def test_payment(request):
    return render(request, 'test_payment.html')


class CreateCheckoutSessionView(View):
    def post(self, request, *args, **kwargs):
        domain = "https://yourdomain.com"
        if settings.DEBUG:
            domain = "http://127.0.0.1:8000"
        checkout_session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=[
                {
                    'quantity': 1,
                    'price_data': {
                        'currency': 'cad',
                        'product_data': {
                            'name': 'T-shirt',
                        },
                        'unit_amount': 50000000,
                    }
                },
            ],
            mode='payment',
            currency='cad',
            success_url=domain + '/success/?session_id={CHECKOUT_SESSION_ID}',
            cancel_url=domain + '/cancel/',
        )
        return redirect(checkout_session.url)


from django.views.generic import TemplateView


class SuccessView(View):
    def get(self, request, *args, **kwargs):
        payment = stripe.checkout.Session.retrieve(request.GET.get('session_id'))
        # TODO get payment_id and complete added payment record in db and change whatever you want
        print('cvccccccccccccccccccccccccccc')
        print(payment.get('id'))
        print(payment.get('payment_intent'))

        return render(request, 'success.html')


class CancelView(TemplateView):
    template_name = "cancel.html"
